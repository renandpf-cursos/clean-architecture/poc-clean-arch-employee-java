package br.com.pupposoft.poc.cleanarch.employee.java.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.pupposoft.poc.cleanarch.employee.java.domain.Employee;
import br.com.pupposoft.poc.cleanarch.employee.usecase.GetUseCase;
import br.com.pupposoft.poc.test.util.databuilder.domain.EmployeeDataBuilder;

@ExtendWith(MockitoExtension.class)
public class EmployeeControllerUnitTest {

	@InjectMocks
	private EmployeeController employeeController;
	
	@Mock
	private GetUseCase<String> getUseCase;
	
	@Test
	public void getWithSucess() {
		
		final Employee employeeExistent = EmployeeDataBuilder.getAny();
		
		doReturn(employeeExistent).when(getUseCase).get(employeeExistent.getId());
		
		final Employee employeeFound = employeeController.get(employeeExistent.getId());
		
		assertEquals(employeeExistent, employeeFound);
	}
}
