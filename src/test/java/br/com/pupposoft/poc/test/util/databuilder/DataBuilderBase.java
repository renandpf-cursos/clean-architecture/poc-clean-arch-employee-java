package br.com.pupposoft.poc.test.util.databuilder;

import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

public abstract class DataBuilderBase {
private static final int AMOUNT_STRING_VALUES = 200;
	
	protected static String[] randomStringValues = null;
	private static void generateStringValues(final int size) {
		randomStringValues = new String[size];
		int length = 10;
	    boolean useLetters = true;
	    boolean useNumbers = true;
	    
	    for (int i = 0; i < randomStringValues.length; i++) {
	    	final String generatedString = RandomStringUtils.random(length, useLetters, useNumbers);
			randomStringValues[i] = generatedString;
		}
	}
	static {
		if(randomStringValues == null) {
			generateStringValues(AMOUNT_STRING_VALUES);
		}
	}
	
	public static String getRandowValue() {
        final Random ran = new Random();
        
        int nxt = ran.nextInt();
        do {
        	nxt = ran.nextInt();
        }while(nxt < 0 || nxt > AMOUNT_STRING_VALUES);
        
        return randomStringValues[nxt];
	}
}
