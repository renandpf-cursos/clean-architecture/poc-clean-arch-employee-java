package br.com.pupposoft.poc.cleanarch.employee.java.controller;

import br.com.pupposoft.poc.cleanarch.employee.java.domain.Employee;
import br.com.pupposoft.poc.cleanarch.employee.usecase.GetUseCase;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@AllArgsConstructor
public class EmployeeController {

	private GetUseCase<String> getUseCase;
	
	public Employee get(final String id) {
		log.trace("Start id={}", id);
		
		final Employee employee = (Employee) this.getUseCase.get(id);
		
		log.trace("End employee={}", employee);
		return employee;
	}
}
