package br.com.pupposoft.poc.cleanarch.employee.java.domain;

import java.time.LocalDate;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class Employee extends br.com.pupposoft.poc.cleanarch.employee.domain.Employee {
	private String id;
	
	public Employee(
			final String id,
			final String cpf, 
			final String firstName, 
			final String lastName, 
			final LocalDate birthDate) {
		
		super(cpf, firstName, lastName, birthDate);
		
		this.id = id;
	}
}
